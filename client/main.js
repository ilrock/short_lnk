import { Meteor } from 'meteor/meteor';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { Tracker } from 'meteor/tracker';

import Signup from './../imports/ui/signup';
import Login from './../imports/ui/login';
import Link from './../imports/ui/link';
import NotFound from './../imports/ui/notFound';

const unauthenticatedPages =['/', '/signup'];
const authenticatedPages = ['/links']

export const history = createBrowserHistory();

const routes = (
    <Router>
        <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/links" component={Link} />
            <Route component={NotFound} />
        </Switch>
    </Router>
);

Tracker.autorun(() => {

    const isAuthenticated = !!Meteor.userId();
    const pathname = history.location.pathname;

    const isUnauthenticatedPage = unauthenticatedPages.includes(pathname);
    const isAuthenticatedPage = authenticatedPages.includes(pathname);

    if(isAuthenticated && isUnauthenticatedPage){
        history.push('/links');
    } else if (!isAuthenticated && isAuthenticatedPage){
        history.push('/');
    }

    console.log('Authenticated?', isAuthenticated);
})

Meteor.startup(() => {
    ReactDOM.render(routes, document.getElementById("app"));
});

