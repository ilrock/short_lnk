import React, { Component } from 'react';
import { Accounts } from 'meteor/accounts-base';
export default class Link extends Component {
    constructor(props){
        super(props);

        this.state = {
            error: ""
        };
    }

    render() {
        return (
            <div>
                <h1> Link </h1>
                <button onClick={this.handleLogout.bind(this)} > Logout </button>
            </div>
        );
    }
    handleLogout (e) {
        Accounts.logout();
    };
}