import React from 'react';
import {Link} from 'react-router-dom';
import { Meteor } from 'meteor/meteor';

export default class Login extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            error: ""
        };
    }

    render(){
        return(
            <div>
                <h1> Login to ShortLnk </h1>
                <form>
                    <input type="email" ref="email" name="email" placeholder="Email"/>
                    <input type="password" ref="password" name="password" placeholder="Password"/>
                    <button onClick={this.onSubmit.bind(this)}> Login </button>
                </form>
                <Link to="/signup"> Don't have an account? </Link>
            </div>
        );
    }

    onSubmit(e){
        e.preventDefault();

        Meteor.loginWithPassword(this.refs.email.value, this.refs.password.value, (err) => {
            console.log("Logged in");
        })
    }
}