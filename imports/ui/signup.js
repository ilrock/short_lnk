import React from 'react';
import { Link } from 'react-router-dom';
import { Accounts } from 'meteor/accounts-base';

export default class Signup extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            error: ""
        };
    }
    render(){
        return(
            <div>
                <h1> Join ShortLnk </h1>
                <form>
                    <input type="email" ref="email" name="email" placeholder="Email"/>
                    <input type="password" ref="password" name="password" placeholder="Password"/>
                    <button onClick={this.onSubmit.bind(this)}> Create Account </button>
                </form>
                <Link to="/"> Have an account? </Link>
            </div>
            
        );
    }

    onSubmit(e){
        e.preventDefault();

        Accounts.createUser({
            email: this.refs.email.value.trim(),
            password: this.refs.password.value.trim()
        }, (err) => {
            console.log('Signup callback', err);
        });
    }
}